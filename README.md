# Livraria

- `npm install` to install dependecies
- `npm start frontend` to start the frontend
- `npm start backend` to start the server backend

access: http://127.0.0.1:8080

Please make sure you have a internet connection.
The database is remote (https://mlab.com/)

# Technologies:
- MongoDB
- ExpressJS
- Javascript