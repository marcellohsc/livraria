const express = require('express');

module.exports = function(server) {
    //API ROUTES
    const router = express.Router();
    server.use('/api', router);

    server.get('/', function(req, res) {
        res.json({"message": "Hello World!"});
    })
    //Book Routes
    const bookService = require('../api/book/bookService');
    bookService.register(router, '/books');
}