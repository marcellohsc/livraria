const restful = require('node-restful');
const mongoose = restful.mongoose;

const bookSchema = new mongoose.Schema({
    title: {type: String, required: true},
    description: {type: String, required: true, default: false},
    price: {type: String, required: true, default: 0},
    author: {type: String, required: true, default: false},
    year: {type: String, required: true, default: false},
    imageUrl: { type: String, required: false, default: 'https://images-na.ssl-images-amazon.com/images/I/51Aj8R6zoQL._SX331_BO1,204,203,200_.jpg'},
    createdAt: {type: Date, default: Date.now}
})

module.exports = restful.model('Book', bookSchema);