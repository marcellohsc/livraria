const Book = require('./book');

Book.methods(['get', 'post', 'put', 'delete']);
Book.updateOptions({ new: true, runValidators: true });

module.exports = Book;